from django.shortcuts import redirect
from django.views.generic import ListView, DetailView, CreateView
from .models import Article, Author


class AuthorDetail(DetailView):

    model = Author


class AuthorsList(ListView):

    model = Author


class ArticleDetail(DetailView):

    model = Article


class ArticlesList(ListView):

    queryset = Article.objects.prefetch_related('authors')


class ArticleCreate(CreateView):

    model = Article
    fields = ['title', 'text', 'authors']


def home(request):
    return redirect('articles', permanent=True)
