from django.db import models
from django.db.models import CharField, TextField, ManyToManyField
from django.urls import reverse


class Author(models.Model):

    first_name = CharField(max_length=30, verbose_name='Имя')
    last_name = CharField(max_length=30, verbose_name='Фамилия')

    class Meta:

        ordering = ['first_name']
        verbose_name = 'Автор'
        verbose_name_plural = 'Авторы'

    def get_absolute_url(self) -> str:
        return reverse('author', args=[self.pk])

    def __str__(self) -> str:
        return f'{self.first_name} {self.last_name}'


class Article(models.Model):

    title = CharField(max_length=255, verbose_name='Заголовок')
    text = TextField(verbose_name='Текст')
    authors = ManyToManyField(Author, verbose_name='Авторы')

    class Meta:

        verbose_name = 'Статья'
        verbose_name_plural = 'Статьи'

    def get_absolute_url(self) -> str:
        return reverse('article', args=[self.pk])

    def __str__(self) -> str:
        return self.title
