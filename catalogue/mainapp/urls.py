from django.urls import path
from .views import (
    ArticleDetail, AuthorDetail, ArticlesList, AuthorsList,
    ArticleCreate, home
    )


urlpatterns = [
    path('', home, name='home'),
    path('article/<int:pk>/', ArticleDetail.as_view(), name='article'),
    path('article/', ArticlesList.as_view(), name='articles'),
    path('author/<int:pk>/', AuthorDetail.as_view(), name='author'),
    path('author/', AuthorsList.as_view(), name='authors'),
    path('article/create/', ArticleCreate.as_view(), name='article_create'),
]
