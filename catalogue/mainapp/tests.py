from django.test import TestCase
from .models import Article, Author


class ArticleTestCase(TestCase):

    def setUp(self) -> None:
        self.article = Article.objects.create(
            title='Заголовок',
            text='Текст текст текст.',
            )
        self.article.save()

    def test_get_absolute_url(self) -> None:
        self.assertEqual(self.article.get_absolute_url(), '/article/1/')

    def test___str__(self) -> None:
        self.assertEqual(self.article.__str__(), 'Заголовок')


class AuthorTestCase(TestCase):

    def setUp(self) -> None:
        self.author = Author.objects.create(
            first_name='Василий',
            last_name='Пупкин',
            )
        self.author.save()

    def test_get_absolute_url(self) -> None:
        self.assertEqual(self.author.get_absolute_url(), '/author/1/')

    def test___str__(self):
        author = Author.objects.first()
        self.assertEqual(author.__str__(), 'Василий Пупкин')
