from typing import Any
from django.core.management.base import BaseCommand
from django.utils.lorem_ipsum import paragraph, words
from random import randint
from mainapp.models import Author, Article  # type: ignore


class Command(BaseCommand):

    help = 'Create objects with random values in mainapp.models'

    def handle(self, *args: Any, **options: Any) -> None:
        old_authors_pk = list(Author.objects.values_list('pk', flat=True))
        old_articles_pk = list(Article.objects.values_list('pk', flat=True))
        new_authors = [
            Author(
                first_name=words(1, common=False).capitalize(),
                last_name=words(1, common=False).capitalize(),
            )
            for i in range(20)
            ]
        new_articles = [
            Article(
                title=words(randint(2, 6), common=False).capitalize(),
                text=paragraph(),
            )
            for i in range(30)
            ]
        Author.objects.bulk_create(new_authors)
        Article.objects.bulk_create(new_articles)
        new_authors_pk = Author.objects.exclude(
            pk__in=old_authors_pk
            ).values_list('pk', flat=True)
        min_new_author_pk = min(new_authors_pk)
        max_new_author_pk = max(new_authors_pk)
        new_articles = Article.objects.exclude(pk__in=old_articles_pk)
        for i in new_articles:
            i.authors.set([
                randint(min_new_author_pk, max_new_author_pk)
                for j in range(randint(1, 3))
                ])
