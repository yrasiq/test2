import requests
from lxml import html
from typing import Any
from django.core.management.base import BaseCommand
from mainapp.models import Author, Article  # type: ignore


class Command(BaseCommand):

    help = 'Create objects with values from rb.ru in mainapp.models'
    protocol = 'https'
    domain = 'rb.ru'
    list_url = '/news/'
    list_selector = '//a[@class="news-item__title"]/@href'
    authors_selector = '//span[@class="article-header__author-name"]/a/text()'
    title_selector = '//h1[@class="article-header__rubric-title"]/text()'
    text_selector_1 = '//div[@class="article__introduction"]/p/text()'
    text_selector_2 = '//div[@class="article__content-block abv"]/p/text()'
    timeout = 30

    def handle(self, *args: Any, **options: Any) -> None:
        url = f'{self.protocol}://{self.domain}'
        news_list = requests.get(url + self.list_url, timeout=self.timeout)
        assert news_list.ok
        news_list_html = html.fromstring(news_list.text)
        news_list_href = news_list_html.xpath(self.list_selector)

        for href in news_list_href:
            news_detail = requests.get(url + href, timeout=self.timeout)
            assert news_detail.ok
            news_detail_html = html.fromstring(news_detail.text)
            authors = news_detail_html.xpath(self.authors_selector)
            assert len(authors) > 0
            title = news_detail_html.xpath(self.title_selector)
            assert len(title) == 1
            title = title[0].replace(u'\xa0', ' ')
            text = '\n\n'.join(
                [
                    news_detail_html.xpath(self.text_selector_1)[0]
                    .replace(u'\xa0', ' ')
                ] + [
                    i.replace(u'\xa0', ' ')
                    for i in news_detail_html.xpath(self.text_selector_2)
                ]
            )
            self.create_obj(authors, title, text)

    def create_obj(self, authors: list, title: str, text: str) -> None:
        if Article.objects.filter(title=title).exists():
            return

        new_article = Article.objects.create(title=title, text=text)
        new_article.save()
        article_authors = []

        for i in authors:
            first_name, last_name = i.split(' ')
            attrs = {'first_name': first_name, 'last_name': last_name}
            if not Author.objects.filter(**attrs).exists():
                author = Author.objects.create(**attrs)
                print(f'Author "{author}" added')
            else:
                author = Author.objects.filter(**attrs).first()
            article_authors.append(author)

        new_article.authors.set(article_authors)
        print(f'Article "{new_article.title}" added')
